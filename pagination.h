#ifndef PAGINATION_H_INCLUDE
#define PAGINATION_H_INCLUDE

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//extern char msg[1096];

#pragma pack(push,1)
struct paginator
{
	uint8_t page;
	struct paginator *nextPagePtr;
};
#pragma pack(pop)

typedef struct paginator PAGINATOR;
typedef PAGINATOR *PAGINATORPTR;

int insert(PAGINATORPTR *sPtr, uint8_t currPage, uint8_t range, uint8_t maximum);

int printPaginator(PAGINATORPTR start, uint8_t currPage, uint8_t range, uint8_t maxPage, char msg[1096]);


#endif // PAGINATION_H_INCLUDE

