/*
 * DO NOT EDIT THIS FILE. Generated by checkmk.
 * Edit the original source file "pagination-test.check" instead.
 */

#include <check.h>

#line 1 "pagination-test.check"
#include "pagination.h"

START_TEST(page2test)
{
#line 4
PAGINATORPTR start = NULL, start1 = NULL, start2 = NULL, start3 = NULL, start4 = NULL, start5 = NULL, start6 = NULL;
char msg[2000] = {0}, msg1[100] = {0}, msg2[100] = {0}, msg3[100] = {0}, msg4[100] = {0}, msg5[100] = {0} ;
	//insert(&start, CURRENT, RANGE);
    fail_unless(insert(&start,2,3,10) == 3, "function1 borked");
    fail_unless(insert(&start1,1,3,10) == 3, "function2 borked");
    fail_unless(insert(&start2,10,0,10) == -1, "function3 borked");
    fail_unless(insert(&start3,10,1,10) == 1, "function4 borked");
	fail_unless(insert(&start4,3,5,10) == 5, "function5 borked");  
	fail_unless(insert(&start5,10,6,20) == 6, "function6 borked");   
	fail_unless(insert(&start6,20,6,20) == 6, "function5 borked");   

    printPaginator(start, 2,3, 10, msg);
    printPaginator(start1, 1,3, 10, msg1);
    printPaginator(start3, 10,1, 10, msg2);
    printPaginator(start4, 3,5, 10, msg3);
 	printPaginator(start5, 10,6, 20, msg4);
 	printPaginator(start6, 20,6, 20, msg5);

    ck_assert_str_eq(msg," 1  [2]  3 << 10" );
    ck_assert_str_eq(msg1," [1]  2  3 << 10" );
    ck_assert_str_eq(msg2,"1 >>  [10] " );
	ck_assert_str_eq(msg3," 1  2  [3]  4  5 << 10" );
	ck_assert_str_eq(msg4,"1 >>  8  9  [10]  11  12  13  << 20" );
	ck_assert_str_eq(msg5,"1 >>  15  16  17  18  19  [20] ");




}
END_TEST

int main(void)
{
    Suite *s1 = suite_create("Core");
    TCase *tc1_1 = tcase_create("Core");
    SRunner *sr = srunner_create(s1);
    int nf;

    suite_add_tcase(s1, tc1_1);
    tcase_add_test(tc1_1, page2test);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf == 0 ? 0 : 1;
}
