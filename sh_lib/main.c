#include "libhello.h"
#include <unistd.h>
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

void ReadFile(char *filename, int numberArray[], int len)
{
	int  i;
	FILE *file;
	file = fopen (filename,"r");

	if (file)
	{
	
		for (i = 0; i < len; i++)
    		{
			fscanf(file, "%2d ", &numberArray[i] );

		}

	fclose(file);
	}
}


int main(int argc, char *argv[])
{
	int rez = 0;
	int len;
	char filename[100];

	while ( (rez = getopt(argc,argv,"i:l:")) != -1)
	{
		switch (rez)
		{
		case 'i':
			printf("Found argument \"%s\".\n" , optarg); 	
 			strcpy(filename,optarg);
			break;
		case 'l': 
			len = atoi(optarg);
			int num[len];
			ReadFile(filename,num, len);
			printf("Result: %d\n",ArrayMin(num, len) );
			break;
	        };
	};
	 
  return 0;
}


