#include "pagination.h"
#include <string.h>


//char msg[1096];

int insert(PAGINATORPTR *startPtr, uint8_t currPage, uint8_t range, uint8_t max_Page)
{
    PAGINATORPTR newPtr;
    int i;
    int count = 0;
    if (range == 1)
    {
        newPtr = (PAGINATORPTR)malloc(sizeof(PAGINATOR));
        newPtr->page = currPage;
        newPtr->nextPagePtr = NULL;
        *startPtr = newPtr;
        count = 1;
    }
    else if ( (range == 0) ||(currPage < 1) || (max_Page < currPage))
	{
		printf("Invalid value\n");
			count = -1;
	}
	else if (currPage == 1)
	{
		for (i = range; i >= currPage; i--)
		{
			count++;
			newPtr = (PAGINATORPTR)malloc(sizeof(PAGINATOR));
	   		if (newPtr != NULL)
			{

	        	newPtr->page = i;
				newPtr->nextPagePtr = *startPtr;
				*startPtr = newPtr;
			}
			else
			{
		   		printf("No memory avaliable");
			}
		}
	}

	else if(currPage == max_Page)
	{
		for (i = max_Page; i > max_Page - range; i--)
    	{
    		count++;
    		newPtr = (PAGINATORPTR)malloc(sizeof(PAGINATOR));
	   		if (newPtr != NULL)
			{
			  	newPtr->page = i;
				newPtr->nextPagePtr = *startPtr;
				*startPtr = newPtr;
			}
			else
			{
				printf("No memory avaliable");
		   	}	
		}
    }
    else 
    {
        for (i = currPage + range/2; i > currPage - range/2 - range%2; i--)
    	{
    		count++;
    		newPtr = (PAGINATORPTR)malloc(sizeof(PAGINATOR));
	   		if (newPtr != NULL)
			{

#ifdef DEBUG
				printf("%d\n", i);
#endif
	        	newPtr->page = i;
				newPtr->nextPagePtr = *startPtr;
				*startPtr = newPtr;
			}
			else
			{
		   		printf("No memory avaliable");
			}
		}
    }
    
    return count;
}

int printPaginator(PAGINATORPTR currPtr, uint8_t currPage, uint8_t range, uint8_t max_Page,char msg[1096])
{
	int len;
	char *msgPtr = msg;

	if (currPtr == NULL)
	{
		printf("Empty\n");
	}

	else if(( currPage == 1) || ((currPage - range/2 - range%2 +1) == 1 ))
	{

		//printf("Your paginator is: \n\n");
		while (currPtr != NULL)
		{
			if (currPtr->page == currPage)
			{
				msgPtr += sprintf(msgPtr, " [%u] ",currPtr->page);
#ifdef DEBUG
				printf(" [%u] ", currPtr->page);
#endif
				if(currPtr->nextPagePtr == NULL)
				{
                    			break;
				}
                else
                {
                    currPtr = currPtr->nextPagePtr;
                }

			}
		msgPtr += sprintf(msgPtr, " %u ",currPtr->page);
#ifdef DEBUG
		printf(" %u ", currPtr->page);
#endif
		currPtr = currPtr->nextPagePtr;
		}
	msgPtr += sprintf(msgPtr, "<< %u", max_Page);
#ifdef DEBUG
		printf("<< %u\n\n", max_Page);
#endif
		//printf("%s", msg);

	}
	else if(currPage == max_Page)
	{
		//printf("Your paginator is: \n\n");
		msgPtr += sprintf(msgPtr, "1 >> ");
#ifdef DEBUG
		printf("1 >> ");
#endif
		while (currPtr != NULL)
		{
			if (currPtr->page == currPage)
			{
				msgPtr += sprintf(msgPtr, " [%u] ",currPtr->page);
#ifdef DEBUG
				printf(" [%u] ", currPtr->page);
#endif
				if(currPtr->nextPagePtr == NULL)
				{
                    			break;
				}
                else
                {
                    currPtr = currPtr->nextPagePtr;
                }

			}
			msgPtr += sprintf(msgPtr, " %u ",currPtr->page);
#ifdef DEBUG
			printf(" %u ", currPtr->page);
#endif
			currPtr = currPtr->nextPagePtr;
		}
		//msgPtr += sprintf(msgPtr, "");
#ifdef DEBUG
		printf("\n");
#endif
		//printf("%s", msg);
	}
//	else if((currPage - range/2 - range%2) == 1 )
	else
	{
		//printf("Your paginator is: \n\n");
		msgPtr += sprintf(msgPtr, "1 >> ");
#ifdef DEBUG
		printf("1 >> ");
#endif
		while (currPtr != NULL)
		{
			if (currPtr->page == currPage)
			{
				msgPtr += sprintf(msgPtr, " [%u] ",currPtr->page);
#ifdef DEBUG
				printf(" [%u] ", currPtr->page);
#endif
				if(currPtr->nextPagePtr == NULL)
				{
                    			break;
				}
                else
                {
                    currPtr = currPtr->nextPagePtr;
                }

			}
			msgPtr += sprintf(msgPtr, " %u ",currPtr->page);
#ifdef DEBUG
			printf(" %u ", currPtr->page);
#endif
			currPtr = currPtr->nextPagePtr;
		}
	msgPtr += sprintf(msgPtr, " << %u", max_Page);
#ifdef DEBUG
		printf("<< %u\n\n", max_Page);
#endif
		//printf("%s", msg);
	}
	len = strlen(msg);
	return len;
}
