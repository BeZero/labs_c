CFLAGS=-Wall
LIBS=-lcheck

all: page2ints

page2ints: main.o pagination.o
	gcc -o page2ints main.o pagination.o

main.o: main.c pagination.h
	gcc $(CFLAGS) -c main.c

pagination.o: pagination.c pagination.h
	gcc $(CFLAGS) -c pagination.c

test: pagination-test
	./pagination-test

pagination-test: pagination-test.o pagination.o
	gcc -o pagination-test pagination.o pagination-test.o $(LIBS) -lrt -lpthread -lm

pagination-test.o: pagination-test.c pagination.h
	gcc $(CFLAGS) -c pagination-test.c

pagination-test.c: pagination-test.check
	checkmk pagination-test.check > pagination-test.c
